<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

final class PassportSeeder extends Seeder
{

    /**
     * @return void
     */
    public function run(): void
    {
        $sqlTemplate = file_get_contents(__DIR__ . '/init_project.sql');
        $sqlQuery = str_replace(
            [
                '%PERSONAL_CLIENT_ID%',
                '%PERSONAL_CLIENT_SECRET%',
                '%CLIENT_ID%',
                '%CLIENT_SECRET%',
                '%TEST_USER_EMAIL%',
                '%TEST_USER_SECRET%',
            ],
            [
                config('service.passport.personal_client_id'),
                config('service.passport.personal_client_secret'),
                config('service.passport.client_id'),
                config('service.passport.client_secret'),
                config('service.passport.test_user_email'),
                config('service.passport.test_user_secret'),
            ],
            $sqlTemplate
        );
        DB::unprepared($sqlQuery);
    }

}
