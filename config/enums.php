<?php
return [
    'message_status' => [
        'IN_PROGRESS' => 'В обратотке',
        'DONE' => 'Готово',
        'ERROR' => 'Ошибка',
    ]
];
