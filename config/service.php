<?php
return [
    'passport' => [
        'login_endpoint' => env('PASSPORT_LOGIN_ENDPOINT'),
        'personal_client_id' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_ID'),
        'personal_client_secret' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET'),
        'client_id' => env('PASSPORT_CLIENT_ID'),
        'client_secret' => env('PASSPORT_CLIENT_SECRET'),
        'grant_type' => env('PASSPORT_GRANT_TYPE'),
        'test_user_email' => env('PASSPORT_TEST_USER_EMAIL'),
        'test_user_secret' => env('PASSPORT_TEST_USER_SECRET'),
        'expiration_time' => env('PASSPORT_TOKEN_EXPIRATION_TIME', 1),//hours
    ],
];
